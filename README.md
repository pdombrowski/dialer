# Dialer
## Instalacja
package.json
```javascript
{
    "dialer": "https://bitbucket.org/pdombrowski/dialer.git"
}
```
## Komponenty
#### ConnectorFactory
Connector jest klasą, która służy do komunikacji z restowym API do wydzwaniania
##### API
- `create(config):Connector <Connector>`
##### Przykład użycia (fake api)

```javascript
const ConnectorFactory = require('dialer').ConnectorFactory
let connector = ConnectorFactory.create()
let result = await connector.call('<number>')
let callId = result.id
```

##### Przykład użycia (real api)

```javascript
const ConnectorFactory = require('dialer').ConnectorFactory
const config = {
	url: '<url_do_api>',
	login: '<login>',
	password: 'password'
}
let connector = ConnectorFactory.create(config)
let result = await connector.call('<number>')
let callId = result.id
```

##### Przykład użycia (fake api)

```javascript
const ConnectorFactory = require('dialer').ConnectorFactory
let connector = ConnectorFactory.create()
let result1 = await connector.call('555555555')
let callId1 = result1.id
let result2 = await connector.call('555555555')
let callId2 = result2.id
let status1 = await connector.getStatus(callId1)
let status2 = await connector.getStatus(callId2)
//jeśli oba statusy są connected
await connector.bridge(callId1, callId2)
```
##### Uzyskiwanie pożądanego statusu połączenia (fake api)
* `0` (np `022222222`) - połączenie zostanie zakończone ze statusem `FAILED`
* `1` (np `122222222`) - połączenie zostanie zakończone ze statusem `BUSY`
* `2` (np `222222222`) - połączenie zostanie zakończone ze statusem `NO ANSWER`
* `> 2` (np `555555555`) - połączenie zakończy się statusem `ANSWERED`
---
#### Connector
Connector jest klasą, która służy do komunikowania się z API
##### API
* `call(<string> Number): Promise <number>`
* `getStatus(<number> callId): Promise <string> [RINGING, CONNECTED, BUSY, FAILED, NO ANSWER, ANSWERED]`
* `bridge(<string> Number, <string> Number): Promise <boolean>`

---
#### CallFactory
CallFactory jest klasą służącą do dzwonienia
##### API
- `constructor(<Connector> connector)`
- `create(<string> number) : Promise <Call>`
##### Przykład użycia

```javascript
const Connector = require('dialer').Connector
const CallFactory = require('dialer').CallFactory
let connector = new Connector(<string> url, <string>login, <string> password) 
let callFactory = new CallFactory(connector)
let call = await CallFactory.create('<number>')
let status = call.getStatus() //RINGING
```
---
#### Call
##### API

- `getStatus() : Promise <string> [RINGING, CONNECTED, ANSWERED, FAILED, NO ANSWER, BUSY]`
- `getId() : Promise <string> id`

#### Bridge
##### API
- `constructor(<Call> call1, <Call> call2, <Connector> connector)`
- `bridge() : <Promise> true | throw Exception`
- `getStatus() : <Promise> <string> [NEW, READY, RINGING, CONNECTED, ANSWERED, FAILED]`
- `canBridge(): <Promise> <boolean>`
##### CONST
- `STATUSES: [NEW, READY, RINGING, CONNECTED, BRIDGED, ANSWERED, FAILED]`

```javascript
let bridge = new Bridge(call1, call2, connector)
console.log(bridge.STATUSES.READY)
```

##### Przykład użycia 1
```javascript
const Connector = require('dialer').Connector
const CallFactory = require('dialer').CallFactory
const Bridge = require('dialer').Bridge

let connector = new Connector(<string> url, <string>login, <string> password)
let callFactory = new CallFactory(connector)
let call1 = await CallFactory.create('<number1>')
let call2 = await CallFactory.create('<number2>')
let bridge = new Bridge(call1, call2, connector)

let canBridge = await bridge.canBridge()
if (canBridge) {
  bridge.bridge()
}
```
##### Przykład użycia 2
```javascript
const Connector = require('dialer').Connector
const CallFactory = require('dialer').CallFactory
const Bridge = require('dialer').Bridge

let connector = new Connector(<string> url, <string>login, <string> password)
let callFactory = new CallFactory(connector)
let call1 = await CallFactory.create('<number1>')
let call2 = await CallFactory.create('<number2>')
let bridge = new Bridge(call1, call2, connector)

let bridgeStatus = await bridge.getStatus()
if (bridgeStatus = bridge.STATUSES.READY) {
  bridge.bridge()
}
```
---
#### Dialer
##### API
- `call(<string> nubmer1, <string> number2) : <promise> Bridge`
- `configure(<object> config)`
##### Przykład użycia (fake api)

```javascript
const Dialer = require('dialer').Dialer;
let bridge = await Dialer.call('<number1>','<number2>')
let status = await bridge.getStatus()
```

##### Przykład użycia (real api)
```javascript
const Dialer = require('dialer').Dialer;
let config = {
	url: 'url',
	login: 'login'.
	password: 'password'
}
Dialer.configure(config)
let bridge = await Dialer.call('<number1>','<number2>')
let status = await bridge.getStatus()
```
