const Connector = require('./connector/Connector')
const FakeConnector = require('./connector/FakeConnector')

function create(config) {
    if(!config) {
        return new FakeConnector()
    }
    return new Connector(config.url, config.login, config.password)
}

module.exports = {
    create: create
}