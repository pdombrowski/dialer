const Number = require('./Number');
const Call = require('./Call');
const debug = require('debug')('Dialer:CallFactory');

function CallFactory(connector) {
    if (typeof connector !== 'object') {
        throw 'connector must be an instance of Connector';
    }
    let _connector = connector;

    this.create = function (number) {
        let _number = new Number(number);
        return _connector.call(_number.toString()).then(function (body) {
            debug('jakie body?', body);
            return new Call(body.id, _number, _connector);
        });
    }
}

module.exports = CallFactory;