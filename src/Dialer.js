const Number = require('./Number');
const CallFactory = require('./CallFactory');
const ConnectorFactory = require('./ConnectorFactory');
const Bridge = require('./Bridge');
const debug = require('debug')('Dialer:Dialer');

let _config = null;

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function waitUntilIsNotPending(bridge) {
    do {
        var bridgeStatus = await bridge.getStatus();
        debug('check status', bridgeStatus);
        if(bridgeStatus === bridge.STATUSES.RINGING) {
            await timeout(500);
        }
    } while(bridgeStatus === bridge.STATUSES.RINGING)
    return bridgeStatus;
}

async function smartBridge(bridge) {
    var isNotPendingStatus = await waitUntilIsNotPending(bridge);
    if(isNotPendingStatus === bridge.STATUSES.READY) {
        return bridge.bridge()
        .catch(function(error) {
            debug('catch', error);
            return false;
        });
    }
    return false;
}

function configure(config) {
    _config = JSON.parse(JSON.stringify(config));
}
  
async function call (number1, number2) {
    let _connector = ConnectorFactory.create(_config)
    let _callFactory = new CallFactory(_connector);
    [call1, call2] = await Promise.all([_callFactory.create(number1), _callFactory.create(number2)])
    let bridge = new Bridge(call1, call2, _connector);
    var bridgeResult = smartBridge(bridge);
    return bridge;
}

module.exports = {
    call: call,
    configure: configure
};