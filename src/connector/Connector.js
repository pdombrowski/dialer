const request = require('request');
const debug = require('debug')('Dialer:Connector');

function Connector(url, login, password) {
    if (!url || !login || !password) {
        throw 'url, login and password are required';
    }
    let _url = url;
    let _login = login;
    let _password = password;

    this.call = function (number) {
        var callParams = {
            url: _url + '/call.php',
            form: {
                login: login,
                pass: password,
                number: number
            }
        };
        return doRequest(callParams)
    }
    this.getStatus = function (callsId) {
        var callParams = {
            url: _url + '/status.php',
            form: {
                login: login,
                pass: password,
                id: callsId
            }
        };
        return doRequest(callParams)
    }

    this.bridge = function (idA, idB) {
        var callParams = {
            url: _url + '/bridge.php',
            form: {
                login: login,
                pass: password,
                ida: idA,
                idb: idB
            }
        };
        return doRequest(callParams)
    }
}

function doRequest(callParams) {
    return new Promise(function (resolve, reject) {
        request.post(callParams, function (err, httpResponse, body) {
            if (err) {
                reject(err);
                return;
            }
            let jsonBody = {};
            try {
                jsonBody = JSON.parse(body);
            } catch(e) {
                debug('nieprawidłowy json, reject')
                reject(e)
                return
            }
            resolve(jsonBody);
        })
    });
}

module.exports = Connector;