const debug = require('debug')('Dialer:Connector');

let _id = 1
let _calls = {}
function getId() {
    return _id++;
}

function wait(ms) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve()
        }, ms)
    }) 
}

function Call(id, number) {
    let _status = 'RINGING'
    this.getId = () => {
        return id
    }
    this.getStatus = () => {
        return _status;
    }
    
    let statusChanger = async () => {
        switch(number[0]) {
            case '1':
                await wait(200)
                _status = 'FAILED'
            break
            case '2':
            await wait(200)
            _status = 'BUSY'
            break
            case '3':
            await wait(5000)
            _status = 'NO ANSWER'
            break
            default:
            await wait(2000)
            _status = 'CONNECTED'
                await wait(6000)
                _status = 'ANSWERED'
            break
        }
    }
    statusChanger()
}

function Connector() {
    this.call = async function (number) {
        let id = getId()
        _calls[id] = new Call(id, number)
        let response =  {
            id: id
        }
        return response

    }
    this.getStatus = async function (callsId) {
        let call = _calls[callsId]
        let response = call ? {status: call.getStatus()} : {success: false}
        return response
    }

    this.bridge = async function (idA, idB) {
        let callA = _calls[idA]
        let callB = _calls[idB]
        let response = (!callA || !callB || callA.getStatus() !== 'CONNECTED' || callB.getStatus() !== 'CONNECTED')
        ? {success: false} : {success: true}
        return response
    }
}

module.exports = Connector;