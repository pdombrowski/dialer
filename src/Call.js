const debug = require('debug')('Dialer:Call');

const STATUSES = {
    RINGING: 'RINGING',
    CONNECTED: 'CONNECTED',
    BRIDGED: 'BRIDGED',
    ANSWERED: 'ANSWERED',
    FAILED: 'FAILED',
    'NO ANSWER': 'NO ANSWER',
    BUSY: 'BUSY'
}

const STATUS_INTERVAL = 500;
 
function Call(id, number, connector) {
    if(!id) {
        throw 'id is required';
    }
    if(typeof connector !== 'object' || connector.constructor.name !== 'Connector') {
        throw 'connector must be an instance of Connector';
    }
    if(typeof number !== 'object' || number.constructor.name !== 'Number') {
        throw 'number must be an instance of Number';
    }
    const _connector = connector;
    const _number = number;
    let _id = id;
    let _status = STATUSES.RINGING;

    let _lastStatusCheck = new Date().getTime();
    let _isStatusOld = function() {
        return new Date().getTime() - _lastStatusCheck > STATUS_INTERVAL;
    }
    let _isStatusFinished = function() {
        return ~[STATUSES.FAILED, STATUSES['NO ANSWER'], STATUSES.ANSWERED, STATUSES.BUSY].indexOf(_status);
    }
    
    let _refreshStatus = function() {
        return connector.getStatus(_id).then(function(response) {
            _status = response.status;
            _lastStatusCheck = new Date().getTime();
            return _status;
        });
    }

    this.getStatus = function() {
        if(!_isStatusFinished() && _isStatusOld()) {
           return _refreshStatus();
        }
        return Promise.resolve(_status);
    }

    this.getId = function() {
        return _id;
    }
}

module.exports = Call;